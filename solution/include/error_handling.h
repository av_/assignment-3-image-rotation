#ifndef ASSIGNMENT_3_IMAGE_ROTATION_ERROR_HANDLING_H
#define ASSIGNMENT_3_IMAGE_ROTATION_ERROR_HANDLING_H

enum program_status {
    OK = 0,
    WRONG_NUMBER_OF_ARGS,
    WRONG_INPUT_PATH,
    WRONG_OUTPUT_PATH,
    NO_MEMORY,
    READ_IMAGE_ERROR,
    WRITE_IMAGE_ERROR,
    FILE_CLOSE_ERROR
};

enum program_status print_error_and_return_status(enum program_status error_code);

enum program_status print_error_with_status(enum program_status error_code, int status);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_ERROR_HANDLING_H
