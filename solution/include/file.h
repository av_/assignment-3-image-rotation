#ifndef IMAGE_TRANSFORMER_FILE_H
#define IMAGE_TRANSFORMER_FILE_H

#include <stdio.h>

enum open_file_status {
    OPEN_OK = 0,
    OPEN_ERROR
};

enum close_file_status {
    CLOSE_OK = 0,
    CLOSE_ERROR
};

enum open_file_status open_file(FILE** file, const char* const file_path, const char* const mode);

enum close_file_status close_file(FILE * file);

#endif //IMAGE_TRANSFORMER_FILE_H
