#ifndef ASSIGNMENT_3_IMAGE_ROTATION_PIXEL_H
#define ASSIGNMENT_3_IMAGE_ROTATION_PIXEL_H

#include <inttypes.h>

#define PIXEL_SIZE 3

struct pixel { uint8_t b, g, r; };

#endif //ASSIGNMENT_3_IMAGE_ROTATION_PIXEL_H
