#include "image.h"
#include "pixel.h"
#include "rotate.h"

#include <malloc.h>
#include <stddef.h>

struct image rotate(struct image* image) {
    struct image rotated = build_image(image->height, image->width);
    for (size_t i = 0; i < image->height; i++) {
        for (size_t j = 0; j < image->width; j++) {
            set_pixel(&rotated, image->height - 1 - i, j, *get_pixel(image, j, i));
        }
    }
    return rotated;
}
