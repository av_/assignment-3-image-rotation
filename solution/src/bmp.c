#include "bmp.h"
#include "image.h"

#include <inttypes.h>
#include <malloc.h>
#include <stdio.h>

#define PADDING 4

#define BMP_SIGNATURE 0x4D42
#define BF_RESERVED 0
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT (8 *  PIXEL_SIZE)

inline static enum read_status validate_header(const struct bmp_header* const header) {
    if (header->bfType != BMP_SIGNATURE) {
        return READ_INVALID_SIGNATURE;
    }
    if (header->bfReserved != BF_RESERVED ||
        header->biPlanes != BI_PLANES ||
        header->biBitCount != BI_BIT_COUNT ||
        header->bOffBits < sizeof (struct bmp_header))
    {
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}

inline static void read_header(FILE* const in, struct bmp_header* const header) {
  fread(header, sizeof(struct bmp_header), 1, in);
}

inline static size_t calculate_byte_width(uint32_t pixel_width) {
    return pixel_width * PIXEL_SIZE;
}

inline static size_t calculate_padding(uint32_t pixel_width) {uint32_t width_mod = ((pixel_width % PADDING) * (PIXEL_SIZE % PADDING)) % PADDING;
    return width_mod == 0 ? 0: (PADDING - width_mod);
}

inline static enum read_status read_data_line(FILE * const in, struct pixel* const data, uint32_t width, size_t padding_size) {
    size_t res = fread(data, PIXEL_SIZE, width, in);
    if (res != width) {
        return READ_INVALID_BITS;
    }
    if (padding_size) {
        if (fseek(in, (long) padding_size, SEEK_CUR)) {
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}

inline static enum read_status read_data(FILE* const in, struct image* img) {
    size_t padding = calculate_padding(img->width);
    for (size_t i = 0; i < img->height; i++) {
        if (read_data_line
        (in, img->data + (i * img->width), img->width, (long) padding)
        != READ_OK) {
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}


enum read_status from_bmp( FILE* const in, struct image* img ) {
    struct bmp_header header = {0};
    read_header(in, &header);
    enum read_status header_read_status = validate_header(&header);
    if (header_read_status != READ_OK) {
        return header_read_status;
    }

    if (header.bOffBits > sizeof(struct bmp_header)) {
        if (fseek(in, (long) (header.bOffBits - sizeof(struct bmp_header)), SEEK_CUR)) {
            return READ_INVALID_BITS;
        }
    }

    uint64_t width = header.biWidth;
    uint64_t height = header.biHeight;

    *img = build_image(width, height);

    if (img->data == NULL) {
        return READ_NO_MEMORY;
    }
    enum read_status read_data_status = read_data(in, img);
    if (read_data_status != READ_OK) {
        free(img->data);
        return read_data_status;
    }

    return READ_OK;
}

inline static uint64_t calculate_byte_data_size(uint32_t width, uint32_t height) {
    return (calculate_byte_width(width) + calculate_padding(width)) * height;
}

inline static struct bmp_header generate_header(uint32_t width, uint32_t height) {
    struct bmp_header header = {0};
    header.bfType = BMP_SIGNATURE;
    header.bfReserved = BF_RESERVED;
    header.biBitCount = BI_BIT_COUNT;
    header.biSize = BI_SIZE;
    header.biPlanes = BI_PLANES;
    header.biWidth = width;
    header.biHeight = height;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSizeImage = calculate_byte_data_size(width, height);
    return header;
}

inline static enum write_status write_header(FILE* const out, struct bmp_header* const header) {
    if (header == NULL) {
        return WRITE_ERROR;
    }
    if (fwrite(header, sizeof(*header), 1, out) != 1) {
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

inline static enum write_status write_padding(FILE* const out, const void* const zero, size_t padding_size) {
    return fwrite(zero, padding_size, 1, out) == 1 ? WRITE_OK : WRITE_ERROR;
}

inline static enum write_status write_line(FILE* const out, struct pixel* const line, void* const padding, uint32_t width, size_t padding_size) {
    if (fwrite(line, sizeof(struct pixel), width, out) != width) {
        return WRITE_ERROR;
    }
    return write_padding(out, padding, padding_size);
}

inline static enum write_status write_data(FILE* const out, const struct image* img) {
    if (img->height == 0 || img->width == 0) return WRITE_OK;
    size_t padding_size = calculate_padding(img->width);
    char* zero = (padding_size) ? calloc(padding_size, 1) : NULL;
    for (size_t i = 0; i < img->height; i++) {
        enum write_status line_write_status = write_line(out, img->data + (img->width * i), zero, img->width, padding_size);
        if (line_write_status != WRITE_OK) {
            free(zero);
            return line_write_status;
        }
    }
    free(zero);
    return WRITE_OK;
}

enum write_status to_bmp( FILE* const out, const struct image * const img ) {
    struct bmp_header header = generate_header(img->width, img->height);
    enum write_status write_header_status = write_header(out, &header);
    if (write_header_status != WRITE_OK) {
        return write_header_status;
    }
    enum write_status write_data_status = write_data(out, img);
    return write_data_status;
}
