#include "error_handling.h"

#include "stdio.h"

char* error_messages[] = {
        [WRONG_NUMBER_OF_ARGS] = "Usage: ./image-transformer <source-image> <transformed-image>",
        [WRONG_INPUT_PATH] = "The first argument must refer to a valid readable input file",
        [WRONG_OUTPUT_PATH] = "The second argument must contain a valid writable output path",
        [NO_MEMORY] = "No enough free memory",
        [READ_IMAGE_ERROR] = "Couldn't read the source file. Error code:",
        [WRITE_IMAGE_ERROR] = "Couldn't save the resulting image. Error code:",
        [FILE_CLOSE_ERROR] = "Couldn't close the file"
};

enum program_status print_error_and_return_status(enum program_status error_code) {
    fprintf(stderr, "%s", error_messages[error_code]);
    return error_code;
}

enum program_status print_error_with_status(enum program_status error_code, int status) {
    fprintf(stderr, "%s %d", error_messages[error_code], status);
    return error_code;
}
