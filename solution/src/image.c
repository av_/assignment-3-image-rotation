#include "image.h"
#include <inttypes.h>
#include <malloc.h>

/*struct image* build_image(struct image* image, uint64_t width, uint64_t height, struct pixel* data) {
    image->width = width;
    image->height = height;
    image->data = data;
    return image;
}*/

struct image build_image(uint64_t width, uint64_t height) {
    struct image image = {0};
    image.width = width;
    image.height = height;
    image.data = malloc(width * height * PIXEL_SIZE);
    return image;
}

void destroy_image(struct image* img) {
    if (img != NULL) {
        if (img->data != NULL) {
            free(img->data);
        }
    }
}

struct pixel* get_pixel(struct image* image, uint64_t column, uint64_t row) {
    if (image == NULL || image->data == NULL) {
        return NULL;
    }
    return &image->data[row * image->width + column];
}

void set_pixel(struct image* img, uint64_t column, uint64_t row, struct pixel value) {
    *get_pixel(img, column, row) = value;
}
