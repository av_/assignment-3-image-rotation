#include "bmp.h"
#include "error_handling.h"
#include "file.h"
#include "image.h"
#include "rotate.h"

int main( int argc, char** argv ) {
    if (argc != 3) {
        return print_error_and_return_status(WRONG_NUMBER_OF_ARGS);
    }

    char* input_filepath = argv[1];
    char* output_filepath = argv[2];

    FILE *input_file, *output_file;

    if (open_file(&input_file, input_filepath, "rb") != OPEN_OK) {
        return print_error_and_return_status(WRONG_INPUT_PATH);
    }

    if (open_file(&output_file, output_filepath, "wb") != OPEN_OK) {
        if (close_file(input_file) != CLOSE_OK) {
            print_error_and_return_status(FILE_CLOSE_ERROR);
        }
        return print_error_and_return_status(WRONG_OUTPUT_PATH);
    }

    struct image img = {0};
    enum read_status read_bmp_status = from_bmp(input_file, &img);

    if (close_file(input_file) != CLOSE_OK) {
        return print_error_and_return_status(FILE_CLOSE_ERROR);
    }

    if (read_bmp_status != READ_OK) {
        destroy_image(&img);
        return print_error_with_status(READ_IMAGE_ERROR, read_bmp_status);
    }

    struct image rotated = rotate(&img);

    enum write_status write_bmp_status = to_bmp(output_file, &rotated);

    destroy_image(&img);
    destroy_image(&rotated);
    if (close_file(output_file) != CLOSE_OK) {
        return print_error_and_return_status(FILE_CLOSE_ERROR);
    }

    if (write_bmp_status != WRITE_OK) {
        return print_error_with_status(WRITE_IMAGE_ERROR, write_bmp_status);
    }

    return OK;
}
